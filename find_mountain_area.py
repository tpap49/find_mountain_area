import sys
import math
import array as arr
from time import sleep

class BasePoint :
    def __init__ (self, x1: float, x2: float) :
        self.x1 = x1
        self.x2 = x2
        
class TopPoint :
    def __init__ (self, x: float, y: float) :
        self.x = x
        self.y = y

    def contains(self, baseP) :
        base = BasePoint(self.x - self.y, self.x + self.y)
        return bool(base.x1 <= baseP.x1 and base.x2 >= baseP.x2)

def base2top(bp) :
    return TopPoint(bp.x1 + (bp.x2 - bp.x1) / 2, (bp.x2 - bp.x1) / 2)

def top2base(tp) :
    return BasePoint(tp.x - tp.y, tp.x + tp.y)

def calcArea(pointVector) :
    if (len(pointVector) == 0) :
        return 0

    area = 0
    cnt = 0

    for vec in pointVector :
        if (cnt == len(pointVector) - 1) :
            break

        p1 = vec
        cnt += 1
        p2 = pointVector[cnt]
        if (p1.y is 0 or p2.y is 0) :
            area += pow( max( p1.y, p2.y ), 2) / 2
        else :
            area += (p1.y + p2.y) * (p2.x - p1.x) / 2.00


    return area

def is_contained(topP_Vec, p) :
    if (len(topP_Vec) == 0) :
        return False

    for tp in topP_Vec :
        if (tp.contains(p)) :
            return True
    
    return False

def intersection(topP1, topP2) :
    x1 = topP1.x
    y1 = topP1.y
    x2 = topP1.y + topP1.x
    y2 = 0
    x3 = topP2.x - topP2.y
    y3 = 0
    x4 = topP2.x
    y4 = topP2.y

    den = ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))
    if (den == 0) : #No intersection
        return TopPoint(0,0)

    x = ((x1*y2 - y1 * x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3 * x4)) / den
    y = ((x1*y2 - y1 * x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3 * x4)) / den
    if (x <= x1 or x >= x2 or y <= 0 or y >= y1) : #No Intersection
        return TopPoint(0,0)

    return TopPoint(x, y)


def sortHeight(bp1) :
    return base2top(bp1).y

def sortLeft2Right(tp1) :
    return top2base(tp1).x1

if __name__ == "__main__":
    basePoints = [BasePoint(9,15), BasePoint(8,14), BasePoint(5,9), BasePoint(0,6), BasePoint(2,12), BasePoint(5,13), BasePoint(16, 21)]

    if (len(basePoints) == 0) :
        print("Total Area: 0")
        sys.exit(0)
    
    if(len(basePoints) == 1) :
        print("Total Area: " + str(pow(basePoints[0].x2 - basePoints[0].x1, 2)/4))
        sys.exit(0)

    #Sort by height
    basePoints.sort(reverse=True, key=sortHeight)
    topPoints = []
    for bp in basePoints :
        if (not is_contained(topPoints, bp)) :
            topPoints.append(base2top(bp))

    topPoints.sort(key=sortLeft2Right)

    finalPoints = []
    finalPoints.append(TopPoint(topPoints[0].x - topPoints[0].y, 0))
    cnt = 0

    for tp in topPoints :
        if (cnt == len(topPoints) - 1) :
            break

        finalPoints.append(tp)
        cnt += 1
        interPoint = intersection(tp, topPoints[cnt])

        if (interPoint == TopPoint(0,0)) :
            finalPoints.append(TopPoint(topPoints[cnt-1].x + topPoints[cnt-1].y, 0))
            finalPoints.append(TopPoint(topPoints[cnt].x - topPoints[cnt].y, 0))
        else :
            finalPoints.append(interPoint)
    
    finalPoints.append(topPoints[len(topPoints) - 1])
    finalPoints.append(TopPoint(topPoints[len(topPoints) - 1].x - topPoints[len(topPoints) - 1].y, 0))

    area = calcArea(finalPoints)
    print("Total Area: " + str(area))
