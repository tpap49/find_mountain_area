You are given a list of N mountains in a 2D landscape, represented as triangles.

The N mountains are defined by :
1) The x position of their bottom - left corner(left ∈ ℕ)
2) The x position of their bottom - right corner(right ∈ ℕ, right > left)
3) Their height(height ∈ ℕ)

The first element in the list mountains[0] represents
the mountain which is the nearest from the observer(you).The last element mountains[N - 1] represents the mountain behind all the others.

# Example input :
mountains = [
{'left': 9, 'right' : 15, 'height' : 3},
{ 'left': 8, 'right' : 14, 'height' : 3 },
{ 'left': 5, 'right' : 9, 'height' : 2 },
{ 'left': 0, 'right' : 6, 'height' : 3 },
{ 'left': 2, 'right' : 12, 'height' : 5 },
{ 'left': 5, 'right' : 13, 'height' : 4 },
]
# Should print 39.25

