#include <chrono>
#include <optional>
#include <iostream>
#include <vector>
#include <algorithm>

struct BasePoints {
	float x1 = 0;
	float x2 = 0;

	BasePoints(float x_1, float x_2) :
		x1(x_1), x2(x_2) {}
};

struct TopPoint {
	float x = 0;
	float y = 0;

	TopPoint(float xp, float yp) :
		x(xp), y(yp) {}

	bool contains(const BasePoint& p2) const {
		auto base = BasePoint(x - y, x + y);
		return (base.x1 <= p2.x1 && base.x2 >= p2.x2);
	}
};

inline TopPoint base2top(const BasePoints& p) {
	return TopPoint(p.x1 + (p.x2 - p.x1) / 2, (p.x2 - p.x1) / 2);
}

inline BasePoints top2base(const TopPoint& p) {
	return BasePoints(p.x - p.y, p.x + p.y);
}

float calcVecArea(const std::vector<TopPoint>& vec) {
	if (vec.size() == 0) 
		return 0;

	float total_area = 0;
	for (int i = 0; i < vec.size() - 1; ++i) {
		const TopPoint& p1 = vec[i];
		const TopPoint& p2 = vec[i+1];
		if (p1.y == 0 || p2.y == 0) {
			total_area +=  std::pow(max(p1.y, p2.y),2)/2; //Half triangle Area if triangle presented
		}
		else {
			total_area += (p1.y + p2.y) * (p2.x - p1.x) / 2.00;
		}
	}
	return total_area;
}

bool is_contained(const std::vector<TopPoint>& vec, const BasePoint& p) {
	if (vec.empty())
		return false;

	for (const auto& t : vec) {
		if (t.contains(p))
			return true;
	}
	return false;
}

inline void append2vector(std::vector<TopPoint>& vec, const BasePoints& p) {
	if (!is_contained(vec, p))
		vec.emplace_back(base2top(p));
}

std::optional<TopPoint> intersection(const TopPoint& p1, const TopPoint& p2) {

	size_t x1 = p1.x;
	size_t y1 = p1.y;
	size_t x2 = p1.y + p1.x;
	size_t y2 = 0;
	size_t x3 = p2.x - p2.y;
	size_t y3 = 0;
	size_t x4 = p2.x;
	size_t y4 = p2.y;

	double den = ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4));
	if (den == 0) { //No intersection
		return {};
	}

	double x = ((x1*y2 - y1 * x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3 * x4)) / den;
	double y = ((x1*y2 - y1 * x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3 * x4)) / den;
	if (x <= x1 || x >= x2 || y <= 0 || y >= y1) { //No Intersection
		return {};
	}

	return { TopPoint(x, y) };
}

//You are given a list of N mountains in a 2D landscape, represented as triangles.
//The N mountains are defined by :
//The x position of their bottom - left corner(left ∈ ℕ)
//The x position of their bottom - right corner(right ∈ ℕ, right > left)
//Their height(height ∈ ℕ)
//The first element in the list mountains[0] represents
//the mountain which is the nearest from the observer(you).The last element mountains[N - 1] represents the mountain behind all the others.

/* # Example input :
mountains = [
{'left': 9, 'right' : 15, 'height' : 3},
{ 'left': 8, 'right' : 14, 'height' : 3 },
{ 'left': 5, 'right' : 9, 'height' : 2 },
{ 'left': 0, 'right' : 6, 'height' : 3 },
{ 'left': 2, 'right' : 12, 'height' : 5 },
{ 'left': 5, 'right' : 13, 'height' : 4 },
]
# Should print 39.25
*/

auto main() -> int
{
	std::vector<BasePoints> vec{BasePoints(9,15), BasePoints(8,14), BasePoints(5,9), BasePoints(0,6), BasePoints(2,12), BasePoints(5,13) };
	std::vector<TopPoint> vecTop;

	//If vec.size == 0 return 0 if size == 1 return triangle area

	auto t0 = std::chrono::high_resolution_clock::now();

	//Sort by height
	std::sort(vec.begin(), vec.end(), [&](BasePoints& p1, BasePoints& p2) { return base2top(p1).y > base2top(p2).y; });

	for (const auto& v : vec) {
		append2vector(vecTop, v);
	}

	//Sort left to right
	std::sort(vecTop.begin(), vecTop.end(), [&](TopPoint& p1, TopPoint& p2) { return top2base(p1).x1 < top2base(p2).x1; });

    std::vector<TopPoint> finalVec;
	finalVec.reserve(vecTop.size());
	finalVec.emplace_back(TopPoint(vecTop.front().x - vecTop.front().y, 0));
	for (int i = 0; i < vecTop.size() - 1; ++i) {
		finalVec.emplace_back(vecTop[i]);
		auto res = intersection(vecTop[i], vecTop[i + 1]);
		if(res.has_value())
			finalVec.emplace_back(res.value());
		else { 
			finalVec.emplace_back(TopPoint(vecTop[i].x + vecTop[i].y, 0)); //Current triangle right intersection with x-axes
			finalVec.emplace_back(TopPoint(vecTop[i+1].x - vecTop[i+1].y, 0)); //Next triangle left intersection with x-axes
		}
	}
	//Last triangle top-point and right intersection with axes
	finalVec.emplace_back(vecTop.back());
	finalVec.emplace_back(TopPoint(vecTop.back().x + vecTop.back().y, 0));

	std::cout << "Total Area: " << calcVecArea(finalVec) << '\n';
	auto t1 = std::chrono::high_resolution_clock::now();
	std::cout << "Excecution time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count() << "ms\n";
}
